#### ES6模块化
##### export
```js
//util1.js
export default {
    a: 100
}
//until2.js
export function fn1() {
    alert('fn1')
}
export function fn2() {
    alert('fn2')
}
```
##### import
```js
//index.js
import util1 from './util1.js'
import {fn1,fn2} from './util2.js'

console.log(util1)
fn1()
fn2()
```
#### class和js构造函数的区别
* class在语法上更加贴合面向对象的写法
* class实现继承更加易读, 易理解
* 更易于写java等后端语言的开发者使用
* 本质还是与语法糖, 使用prototype
#### Promis
* new Promise实例, 而且要return
* new Promise时要传入参数, 函数有resolve reject两个参数
* 成功时执行resolve() 失败时执行reject()
* then监听结果
#### 单线程和异步
* 单线程 - 只有一个线程, 只能做一件事
* 原因 - 避免DOM渲染冲突
* 解决方案 - 异步
##### 单线程原因(避免渲染冲突)
* 浏览器需要渲染DOM
* JS可以修改DOM
* JS执行的时候, 浏览器DOM会暂停渲染
* 两段JS也不能同时渲染(都修改DOM就冲突了)
* webworker支持多线程, 但是不能访问DOM
##### 事件轮询
* js实现异步的具体解决方案
* 同步代码直接执行
* 异步函数直接放到异步队列中
* 等待同步函数执行完毕, 轮询执行异步队列中的函数
#### MVVM
* MVVM - Model View ViewModel
* Model - 模型, 视图
* View - 视图, 模型(视图和模型是分离的)
* ViewModel - 连接Model和View
![prototype](./../images/vue-mvvm.png)
#### Vue三要素
* 响应式：vue如何监听到data每个属性的变化
* 模板引擎：vue的模板如何被解析, 指令如何处理
* 渲染：vue的模板如何被渲染成html？已经渲染过程
##### 响应式 (修改data属性后，vue立刻监听到)
* Object.defineProperty
* data属性被代理到vm上
##### 模板引擎
###### 模板是什么
* 本质：字符串
* 有逻辑, 如v-if v-for等
* 与html格式很相似，但有很大区别
* 最终要转化成html
* 模板最终要转成JS代码：1. 有逻辑(v-if v-for), 必须用JS才能实现 2. 转换成html渲染页面, 必须用JS才能实现, 模板最终转换成一个JS函数(render)
##### 渲染
* render函数是由模板生成的js代码(with语法)
* render函数返回的是vnode
##### render函数和vdom
* updateComponent中现实了vdom的patch
* 页面首次渲染时执行updateComponent(直接patch到container)
* data中每次修改属性, 执行updateComponent(patch通过diff算法修改dom)
##### vue整个实现流程
* 解析模板成render函数
  * with, vm._c, vm._l, vm._s, vm._v
  * 模板中信息都被render函数包含
  * 模板中用到的data中的属性, 都成了JS变量
  * 模板中的v-model v-for v-on都成了JS逻辑
  * render函数返回vnode
* 响应式开始监听
  * Object.defineProperty
  * 将data的属性代理到vm上
* 首次渲染, 显示页面, 且绑定依赖
  * 初次渲染, 执行updateComponent, 执行vm._render()
  * 执行render函数, 会访问到vm.list和vm.title
  * 会被响应式get方法监听到
  * 执行updateComponent, 会走到vdom中的patch方法
  * patch将vnode渲染成dom, 初次渲染完成
* data属性变化, 触发render
  * 修改属性, 被响应式的set监听到
  * set中执行updateComponent
  * updateComponent重新执行vm._render()
  * 生成vnode和preVnode, 通过patch进行对比