### 计算属性和方法
计算属性与方法最主要的区别在于计算属性是基于它们的依赖进行缓存的，只要计算属性的依赖项没有发生改变，计算属性就返回之前的值。
```html
<div id="app">
    <br> {{msg}}
    <br> {{msg}}
    <br> +++++++++++++++++++++++
    <br> {{getmsg()}}
    <br> {{getmsg()}}
</div>
<script src="vue.js"></script>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            msg1: 'computed',
            msg2: 'method',
        },
        computed: {
            msg: function () {
                return this.msg1 + ' ' + Math.random()
            }
        },
        methods: {
            getmsg: function () {
                return this.msg2 + ' ' + Math.random()
            }
        }
    })
</script>
```
###### 1. 首次渲染 outptut:
    computed 0.29711724109570525 
    computed 0.29711724109570525 
    ++++++++++++++++++++++++++++
    method 0.70480519181501 
    method 0.48929781818790175
###### 2. 将msg1改为"computed1" output:
    computed1 0.9596006558481616 // 依赖项发生改变，计算属性msg重新计算
    computed1 0.9596006558481616 // DOM更新，但是计算属性msg返回相同值
    +++++++++++++++++++++++ 
    method 0.4970903961641815 
    method 0.6135666256315564 // msg1的改变引起渲染使getmsg方法重新调用了两次
###### 3. 将msg2改为"method1" output:
    computed1 0.9596006558481616 
    computed1 0.9596006558481616 // msg2改变引起渲染并未导致计算属性msg重新计算
    +++++++++++++++++++++++ 
    method1 0.21990289157608167 
    method1 0.6273221979857562 // getmsg方法依旧调用了两次

结论：
1. 对于计算属性，多次调用只会计算一次，只有依赖项发生改变，才会重新计算。
2. 对于方法，每次调用都会重新计算，重新渲染也会引起重新计算。