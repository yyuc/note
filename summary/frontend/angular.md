### 特性
* MVC
* 模块化和依赖注入
* 双向数据绑定
* 指令
### Angular初始化
* 自动初始化
  Angular 在以下两种情况下自动初始化，一个是在 DOMContentLoaded 事件触发时，或者在 angular.js 脚本被执行的同时如果 document.readyState 被置为 'complete' 的话。初始化时，Angular 会去找 ng-app 这个指明应用开始所在的指令。如果 ng-app 指令被找到的话，Angular 会做以下几件事：
  * 加载 ng-app 指令所指定的 模块
  * 创建应用所需的 injector
  * 以 ng-app 所在的节点为根节点，开始遍历并编译DOM树（ng-app 指出了应用的哪一部份开始时 Angular 去编译的）

* 手动初始化
  如果你想在初始化阶段拥有更多的控制权，你可以使用手动方法启动应用。你需要手动启动的可能会是你想要在你的应用中使用脚本加载器，或者你可能想要在 Angular 编译页面之前执行一些别的操作。
  ![Angular Init](./../images/concepts-startup.png)
  下面是一个手动初始化 Angular 的例子：
  ``` html
  <!doctype html>
  <html xmlns:ng="http://angularjs.org">
    <body>
      Hello World!
      <script src="http://code.angularjs.org/angular.js"></script>
      <script>
         angular.element(document).ready(function() {
           angular.module('myApp', []);
           angular.bootstrap(document, ['myApp']);
         });
      </script>
    </body>
  </html>
  ```
  注意，在上面的例子中，我们提供了我们应用要加载的模块名作为 api/angular.bootstrap 函数的第二个参数。需要注意的是 angular.bootstrap 不会凭空创建模块，在我们将模块作为参数注入之前，必须创建任一自定义的 模块。
  以下是你的 Angular 代码运行时遵循的顺序：
  在HTML页面以及所有代码加载完毕后，Angular 会去找到应用的根元素（通常是文档的根节点）
  调用 api/angular.bootstrap 去 编译 各元素成为一个可执行的且双向绑定的应用

* 延迟启动
  这个特色可以让像 Batarang 一样的测试工具横插一杠进入 Angular 的引导进程，并且溜进模块中的DI注册机制中，这样就可以替换或者增强DI提供的服务。（译注：由于后面一句不甚理解其原理，不敢贸然翻译）
  当 api/angular.bootstrap 被调用时，如果 window.name 包含 NG_DEFER_BOOTSTRAP! 前缀，引导进程会被暂停直到 angular.resumeBootstrap() 被调用。
  angular.resumeBootstrap() 以一个可选的数组作为参数。这个数组是包含了应用启动时需要被注入的模块。
### Controller注意点
 * 不要试图复用Controller, 一个控制器一般只负责一小块试图
* 不要在Controller中操作DOM, 这不是控制器的职责
* 不要在Controller里面做数据格式化, ng有很好用的表单控件
* 不要在Controller里面做数据过滤, ng有$filter服务
* 一般来说, Contoller是不会相互调用打, 控制器之间的交互会通过事件进行
### \$emit, \$broadcast
* $emit向上传播
* $broadcase向下传播
### $scope
* \$scope是一个POJO (Plain Old JavaScript Object)
* \$scope提供了一些工具方法\$watch()/\$apply()
* \$scope是表达式的执行环境(或者叫做作用域)
* \$scope是一个树型结构, 与DOM标签平行
* 子\$scope对象会继承父\$scope上的属性和方法
* 每一个angular应用只有一个根\$scope对象(一般位于ng-app上)
* \$scope可以传播事件, 类似DOM事件, 可以向上也可以向下
* $scope不仅是MVC的基础, 也是实现双向数据绑定的基础
* 可以用angular.element(\$0).scope()获取选中元素的scope
#### \$scope的生命周期
Creation(创建) -> Watcher registration(注册监控) -> Model mutation(数据模型变化) -> Mutatioin observation(数据模型变化检测) -> Scope destruction(销毁)
  1. 创建: AngularJS启动时, 会使用\$injector创建一个根作用域, 将作用域传进相应的控制器或指令中
  注意: AngularJS除了ng-controller和ng-repeat指令会创建自己的子作用域, 一般不会创建自己的 $scope
  2. 链接(注册观察者): AngularJS运行时, 指令会创建自己的作用域, 所有的 \$scope对象都会链接到视图上, 通过注册 $watch函数来获取数据变化通知
  3. 模型状态改变: 更新模型状态必须发生在scope.\$apply方法中才会被观察到. Angular框架封装了\$apply过程, 无需我们操心.
  4. 更新: AngularJS通过在顶层 \$scope对象执行事件循环, 每个自作用域都会执行自己的脏值检测(\$digest), 每个监控函数会检查变化，如果检测到变化, 则 $scope对象触发指定的回调函数
  5. 销毁: 当不再需要子作用域时, \$socpe上可以通过使用 \$destoy()方法销毁作用域, 回收3资源。
### ngRouter
```js
angular.module('app', []).config(function ($routeProvider) {
    $routeProvider.when('/hello', {
        templateurl: 'tpls/hello.html',
        controller: 'HelloCtrl'
    }).when('/list', {
        templateurl: 'tpls/booklist.html',
        controller: 'BookListCtrl'
    }).otherwise({
        redirectTo: '/hello'
    })
})
```
### ng官方推荐打模块切分方式
![angular module](./../images/app.png)
* 任何一个ng应用都是由控制器, 指令, 路由, 过滤器等有限的模块类型构成的
* 控制器, 指令, 服务, 路由, 过滤器分别放在一个模块里(可以借助构建工具合并)
* 用一个总的app模块作为入口点, 它依赖其它所有模块
### 指令
#### 匹配模式
  * E 元素
  * A 属性(默认)
  * M 样式类
  * C 注释
  推荐使用元素和属性打方式使用指令
  当需要创建带有自己模板打指令时, 使用元素名称的方式创建指令
  当需要为已有HTML标签增加功能时, 使用属性的方式创建指令

### AngularJS指令执行机制
  ![directive](./../images/directive.png)
  #### 加载阶段
  * 加载angular.js, 找到ng-app指令, 确定应用边界
  #### 编译阶段
  * 遍历DOM, 找到所有指令, 然后缓存到缓存里面去
  * 遍历DOM, 找到所有指令, 然后缓存到缓存里面去
  * 根据指令代码中打template, replace, transclude对DOM结构进行一些转变
  * 如果存在compile函数, 则去调用
  #### 链接阶段
  * 对每一条指令运行link函数
  * link函数一般用来操作DOM, 绑定事件监听器
### require
  在自定义Angular指令时, 其中有一个叫做require的字段, 这个字段的作用是用于指令之间的相互交流. 举个简单的例子, 假如我们现在需要编写两 个指令，在linking函数中有很多重合的方法, 为了避免重复自己(著名的DRY原则), 我们可以将这个重复的方法写在第三个指令的 controller中, 然后在另外两个需要的指令中require这个拥有controller字段的指令, 最后通过linking函数的第四个参数就可以引用这些重合的方法
  require - 请求另外的controller, 传入当前directive的link function中. require需要传入一个directive controller的名称. 如果找不到这个名称对应的controller, 那么将会抛出一个error。名称可以加入以下前缀：
  * ? - 不要抛出异常. 这使这个依赖变为一个可选项
  * ^ - 允许查找父元素的controller
### scope绑定策略
  * @ 单向绑定, 外部scope能够影响内部scope, 但反过来不成立
  * = 双向绑定, 外部scope和内部scope的model能够相互改变
  * & 对父级作用域进行绑定, 并将其中的属性包装成一个函数
### Service的特性
 * Service都是单例的
 * Service由$injector负责实例化
 * Service在整个应用生命周期中存在, 可以用来共享数据
 * 在需要的地方利用依赖注入机制注入Service
 * 自定义的Service需要写在内置的Service后面
 * 内置Service的命名以$符号开头, 自定义Service应该避免
### Service, Provider, Factory
 * Service, Provider, Factory本质上都是Provider
 * Provider模式是"策略模式"+"抽象工厂模式"的混合体
 * Service是一个可注入的构造器, 在Angularjs中是单例的, 整个应用生命周期只有一份, 用它在controller中通信或者共享数据都很合适
 * Factory是一个可注入的方法, 与service的区别就是: factory是普通的function, 而service是一个constructor, 这样Angular在调用service时会调用new关键字, 而调用factory只是调用普通的function, 所以factory可以返回任何东西, 而service不可以返回
 * Service和Factory都是provider的封装, provider必须提供一个$get方法, 当然也可以说provider是一个可配置的factory