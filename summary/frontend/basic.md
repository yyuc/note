### 基础概念
#### 变量类型
JavaScript按照存储方式将变量分为值类型和引用类型
#### typeof
object, number, boolean, string, function, undefined
```js
typeof 1 === 'number'
typeof true === 'boolean'
typeof 'test' === 'string'
typeof undefined === 'undefined'
typeof null==='object'
typeof [] === 'object'
typeof {} === 'object'
typeof Object === 'object'
typeof function(){} === 'function'
```
#### JSON
1. 一种数据格式
2. 是JavaScript的内置对象(同Math)
#### 原型链
1. 所有的引用类型, 都具有对象特性,即自由可扩展性(除了null)
2. 所有的对象都具有一个__proto__的属性, 即隐式原型属性,其属性值是一个普通对象
3. 所有的函数对象都有一个prototype属性, 其属性值也是一个普通对象
4. 所有的对象的__proto__属性值指向它的构造函数的prototype属性值
5. 当试图得到一个对象的某个属性时, 如果这个对象本身没有这个属性, 那么会去它的__proto__(即它的构造函数的prototype)中寻找

![prototype](./../images/prototype.jpg)
#### new(执行步骤)
1. 创建空的对象并赋给this
2. 给this赋值属性
3. 返回this
#### this
1. 构造函数 (this是构造的对象)
2. 作为普通对象属性运行 (this是普通对象)
3. 作为普通函数运行 (this是全局对象)
4. call, apply, bind (this是绑定传入的对象)
#### 执行上下文(变量提升)
1. 范围：一段\<script\>或者一个函数
   \<script\>生成全局上下文,函数生成函数上下文, 变量提升只是把声明提前, 但是并不赋值
2. 全局：变量定义, 函数声明
   执行之前把变量定义和函数声明提前
3. 函数：变量定义, 函数声明, this, arguments
   函数执行之前把变量定义, 函数声明, this, arguments提前
   ```js
   function fn(){} //函数声明
   var fn = function(){} //函数表达式
   ```
#### 作用域
ES6之前只有全局作用域和函数作用域, ES6里加入了块级作用域需配合let关键字使用
* 自由变量
  在当前作用域里没有定义的变量, 就是自由变量, 自由变量向上在父级作用域（函数定义时的作用域）中查找
* 作用域链, 即自由变量的查找
  一个自由变量沿着父级作用域逐层向上查找形成的链式结构
* 闭包的场景(闭包实际应用中主要用于封装变量, 收敛权限)
  1. 函数作为返回值
  2. 函数作为参数传递
  ```js
  function isFirstLoad() {
      var _list = []
      return funciton(id) {
          if (_list.indexOf(id) >= 0) {
              return false
          } else {
              _list.push(id)
              return true
          }
      }
  }
  var firstLoad = isFirstLoad()
  firstLoad(10) //true
  firstLoad(10) //false
  firstLoad(20) //true
  ```
#### 异步
同步会阻塞操作,异步不会阻塞操作
(alert是同步, setTimeout是异步)
##### 异步场景(需要等待的情况下就需要异步)
1. 定时任务：setTimeout, setIntervel
2. 网络请求：ajax请求, 动态\<img\>加载
```js
// Ajax请求
console.log('start')
$.get('./data1.json', function (data1) {
    console.log(data1)
})
console.log('end')

// 动态加载图片
console.log('start')
var img = document.createElement('img')
img.onload = function () {
    console.log('loaded')
}
img.src = '/xxx.png'
console.log('end')
```
3. 事件绑定
```js
console.log('start')
document.getElementById('btn1').addEventListener('click', function () {
    console.log('clicked')
})
console.log('end')
```
#### JavaScript-Web-API
1. JavaScript基础语法知识：ECMA(262)标准
2. JavaScript-Web-API：W3C标准
   * DOM操作
   * BOM操作
   * 事件绑定
   * Ajax请求(包括HTTP协议)
   * 存储
#### DOM
浏览器把拿到的html代码, 结构化成一个浏览器能识别并且JavaScript能操作的模型
##### DOM常用操作API
1. 获取DOM节点, 以及节点的property和attribute
2. 获取副节点, 获取子节点
3. 新增节点, 删除节点
##### DOM节点的attribute和property的区别
* property是一个JavaScript对象的属性的修改
* attribute是对html标签属性的修改
#### 事件
##### 使用事件代理
* 代码比较简洁
* 降低浏览器压力, 提高性能
#### XMLHttpRequest
```js
var xhr = new XMLHttpRequest()
xhr.open("GET", "/api", false)
xhr.onreadystatechange = function () {
    if (xhr.readystate === 4) {
        if (xhr.status == 200) {
            alert(xhr.responseText)
        }
    }
}
xhr.send(null)
```
##### 状态码
###### readystate
* 0 - (为初始化)还没有调用send方法
* 1 - (载入)已调用send()方法, 正在发送请求
* 2 - (载入完成)send()方法执行完成, 已经接收到全部响应内容
* 3 - (交互)正在解析响应内容
* 4 - (完成)响应内容解析完成，可以在客户端调用了
###### satus
* 2xx - 表示成功处理请求. 如200
* 3xx - 需要重定向, 浏览器直接跳转
* 4xx - 客户端请求错误, 如404
* 5xx - 服务端错误
##### 跨域
浏览器有同源策略, 不允许Ajax访问其他域接口
夸域条件：协议, 域名, 端口, 有一个不同就算跨域
##### 跨域方式
* 客户端 jsonp
* 服务端 http header
##### 三个允许跨域加载资源的标签
* \<img src="xxx"\> (用于打点统计,统计网站可能是其他域)
* \<link href="xxx"\> (可以使用CDN)
* \<script src="xxx"\> (可以使用JSONP)
##### 跨域注意事项
* 所有跨域请求都必须经过信息提供方允许
* 如果未经允许即可获取, 那是浏览器同源策略出现漏洞
##### JSONP实现原理
* 加载http://www.xxx.com/xx.html
* 不一定服务器端真正有一个xx.html文件
* 服务器端可以根据请求, 动态生成一个文件并返回
* 同理于\<script src="http://www.xxx.com/xx.js"\>

```html
    <script>
        window.callback = funciton(data) {
            //跨域得到的信息
            console.log(data)
        }
    </script>
    <script src="http://www.xxx.com/xx.js">
    <!-- 以上将返回callback({x:100, y:200}) -->
```
##### 服务端设置http header
* 另外一个解决跨域的简洁方法, 需要服务端实现
* 是将来解决跨域问题的一个趋势
```java
//第二个参数写允许跨域的域名称, 不建议直接写"*"
response.setHeader("Access-Control-Allow-Origin", "http;//a.com,http;//b.com"）
response.setHeader("Access-Control-Allow-Headers", "X-Requested-With")
response.setHeader("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS")

//接收跨域的cookie
response.setHeader("Access-Control-Allow-Credentials", "true")
```
#### cookie, sessionStorage, localStorage
##### cookie
* 用于客户端和服务端通信
* 有本地存储功能, 经常被"借用"
* 使用document.cookie = ...可以获取和修改
##### cookie缺点
* 存储量太小, 只有4kb
* 所有http请求都带着, 会影响获取资源的效率
* API简单, 需要封装才能用document.cookie = ...
##### sessionStorage, localStorage
* HTML5专门为存储设计, 最大容量为5M
* API简单易用
* localStorage.setItem(key, value), localStorage.getItem(key)
* 建议统一使用try-catch包装
##### defer & async

#### AMD
require.js
* 全局define函数
* 全局require函数
* 依赖JS会自动, 异步加载

#### CommonJS
* nodejs模块化规范,大量被前端使用
* 前端开发依赖的插件和库, 都可以从npm中获取
* 构建工具的高度自动化, 使得使用npm的成本非常低
* commonjs不会异步加载js, 而是同步一次性加载出来

#### AMD和CommonJS的使用场景
* 需要异步加载JS, 使用AMD
* 使用了npm建议使用CommonJS
#### 输入url到得到html的过程
##### 加载资源的形式
* 输入url(跳转页面)加载html
* 加载html中的静态资源(js/css/image/fonts)
##### 加载资源的过程
* 浏览器根据DNS服务器得到域名的ip地址
* 向这个IP的机器发送http请求
* 服务器收到, 处理并返回http请求
* 浏览器得到返回内容
##### 浏览器渲染页面过程
* 根据HTML结构解析生成DOM Tree
* 根据CSS生成CSS Rule Tree
* 将DOM Tree和CSS Rule Tree整合生成Render Tree
* 根据Render Tree开始渲染和展示
* 遇到\<script\>时, 会执行并阻塞渲染
#### window.onload和DOMContentLoaded
* window.load - 在页面的全部资源加载完才会执行, 包括图片, 视频等
* DOMContentLoaded - DOM渲染完即可执行, 此时图片, 视频可能还没有加载完
#### 性能优化
##### 原则
* 多使用内存, 缓存
* 减少CPU计算, 减少网络
##### 从哪里入手
* 加载页面和静态资源
  1. 静态资源的压缩合并
  2. 静态资源缓存
     * 通过连接名称控制缓存
     * ETag/Cache-Control
     * 服务端代理缓存
  3. 使用CDN让资源加载更快
  4. 使用SSR后端渲染, 数据直接输出到HTML中
* 页面渲染
  1. CSS放前面, JS放后面
  2. 懒加载(图片懒加载, 下拉懒加载)
     ```html
     <img id="img1" src="preview.png" data-realsrc="abc.png"/>
     <script type="text/javascript">
        var img1 = document.getElementById('img1')
        img1.src = img1.getAttribute('data-realsrc')
     </script>
     ```
  3. 减少DOM查询, 对DOM查询做缓存
  4. 减少DOM操作, 多个操作尽量合并在一起执行
  5. 事件节流
     ```js
     var textarea = document.getElementById('text')
     var timeoutId
     textarea.addEventListener('keyup', function () {
        if (timeoutId) {
            clearTimeOut(timeoutId)
        }
        timeoutId = setTimeout(function () {
            // 触发change事件
        }, 100)
     } )
     ```
  6. 尽早执行操作(如DOMContentLoaded)
#### 安全性
##### XSS跨站点请求攻击
* 替换关键字
##### XSRF跨站点请求伪造
* 增加验证流程