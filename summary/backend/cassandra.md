## Table
  #### Column definitions
  ###### Static Column
  1. tables with the **COMPACT STORAGE** option cannot use them.
  2. a table without clustering columns cannot have static columns(in a table without clustering columns, every partition has only one row, and so every column in inherently static).
  3. only non **KEY columns** can be static.
  ###### Primary Key
  A CQL primary key is composed of 2 parts:
  1. the partition key part. it is the component of the primary key definition. It can be a single column or, using additional parenthesis, can be multiple columns. A table always have at least a partition key, the smallest possible table definition is:
      ```sql
      CREATE TABLE t(k text PRIMARY KEY);
      ```
  2. the clustering columns. Those are the columns after the first component of the primary key definition, and the order of those columns define the clustering order.

  ##### Group
  * 分组（GROUP BY）可以将具有相同列值的一组行数据压缩在同一行显示。
  * 使用（GROUP BY）只能在pertition key的级别上或者在clustering column的级别上对行进行分组。所以GROUP BY只接受primary key定义顺序的列名作为参数。如果一个primary key列被应用于相等过滤，但是该列不一定要出现在GROUP BY语句中。
  * 聚合函数会对每个分组产生一个独立的值。如果没有使用GROUP BY，那么聚合函数会对所有的行产生一个值。
  * 如果选择一个使用聚合函数的列，在GROUP BY语句中，会返回第一个遇到的列值。（使用*返回的是最后一个遇到的值？？？）

  ##### Order
  ORDER BY语句可以用来给返回值进行排序。它使用由列名和该列的顺序构成的列表作为参数（ASC表示升序，DSC表示降序，默认值是ASC升序）。当前版本上可用的排序受到表上定义的clustering order制约：
  * 如果表上未定义任何的clustering order，那么只能允许与clustering column隐形的顺序全部相同的顺序或者全部相反的顺序。
  * 否则只允许与clustering order定义的排序全部的都相同，或者全部的都相反。

  ##### Limit
  LIMIT用来限制查询返回的行数，PER PARTITION LIMIT用来限制特定的partition查询返回的行数。

  ##### Allowing Filtering
  默认地，CQL仅支持没有服务端filtering的查询，因为可以预测没有filtering的查询的性能也就是说查询的执行时间跟查询返回的数据量成正比的。
  ALLOW FILTERING允许那些需要filtering的查询。要注意一个使用了ALLOW FILTERING的查询可能会拥有不可预测的性能。即使一个查询只选择了少数的记录也有可能表现出其性能依赖于集群中所有数据。
  例如，考虑下面存储用户信息的表，表里有出生年份和住址所在国家：
  ```sql
  CREATE TABLE users (
    username text PRIMARY KEY,
    firstname text,
    lastname text,
    birth_year int,
    country text
  )

  CREATE INDEX ON users(birth_year);
  ```
  下面的查询是合法的：
  ```sql
  SELECT * FROM users;
  SELECT * FROM users WHERE birth_year = 1981;
  ```
  在这两个例子中，Cassandra保证这些查询的性能会和返回的数据量成正比。特别是如果没有用户在1981年出生，那么第二个查询的性能将不依赖于在数据库中存储的用户信息数量(至少没有直接的关系：由于secondary index的实现，这个查询可能还是依赖于集群中节点的数量，间接的依赖存储的数据量。然而，节点的数量总是比存储的用户数量小好几个数量级)。当然也有可能两个查询都返回非常大的结果，但是返回的结果数量总是可以通过添加LIMIT控制。
  然而，下面的查询会被拒绝：
  ```sql
  SELECT * FROM users WHERE birth_year = 1981 AND country = 'FR';
  ```
  因为Cassandra不能保证他不会去扫描很大数据量的数据即使查询的结果可能很小。通常需要扫描所有在1981年出生的用户的索引实体即使只有少数来自法国。但是如果你知道你在做什么，你可以强制执行这个查询通过使用ALLOW FILTERING，所以下面的语句是合法的：
  ```sql
  SELECT * FROM users WHERE birth_year = 1981 AND country = 'FR' ALLOW FILTERING;
  ```